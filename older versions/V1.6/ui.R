#
# This is the user-interface definition of a Shiny web application. You can
# run the application by clicking 'Run App' above.
#
# Find out more about building applications with Shiny here:
# 
#    http://shiny.rstudio.com/
#
library(shiny)
library(dplyr)
library(forecast)
library(lubridate)
library(shinydashboard)
library(corrplot)
library(networkD3)
library(lattice)
library(outliers)
#library(shinyjs)
library(car)
shinyUI(
  # fluidPage(
  #   htmlTemplate("home.html")    
  # )

  dashboardPage(
   dashboardHeader(title = "AnalyticsToolV1.2",
                   dropdownMenuOutput("logoutMenu")
),
   dashboardSidebar(
     sidebarMenu(
       sidebarSearchForm(textId = "searchText", buttonId = "searchButton",
                         label = "Search..."),
       menuItem("Home",tabName = "home",icon = icon("home")),
       menuItem("File Upload",tabName = "fileUpload",icon = icon("upload")),
       menuItem("EDA", 
                menuSubItem("Analysis", tabName = "eda"),
                menuSubItem("Correlation", tabName = "corChartEda"),
                menuSubItem("Outlier", tabName = "outlierEda"),
                menuSubItem("Seasonality", tabName = "seasonEda")
                ,icon = icon("line-chart")),
       menuItem("Modelling",
                menuItem("Prediction", 
                            menuSubItem("Linear", tabName = "linearPred"),
                         menuSubItem("Decision Tree", tabName = "dtPred"),
                         menuSubItem("Random Forest", tabName = "rfPred"),
                         menuSubItem("Neural Network", tabName = "nnPred"),
                         menuSubItem("XG Boost", tabName = "xgPred")
                          ),
                menuItem("Logistic",
                         menuSubItem("Logistic Regression", tabName = "logicLog"),
                         menuSubItem("Linear", tabName = "linearLog"),
                         menuSubItem("Decision Tree", tabName = "dtLog"),
                         menuSubItem("Random Forest", tabName = "rfLog"),
                         menuSubItem("Neural Network", tabName = "nnLog"),
                         menuSubItem("XG Boost", tabName = "xgLog")
                         ),
                icon = icon("code")),
       menuItem("Setting",
                menuSubItem("Profile", tabName = "settings", icon = icon("user-circle-o")),
                icon = icon("cogs"))
     )#,
    # actionLink("killSession","Kill Process",icon=icon("stop-circle-o"))
   ),
   dashboardBody(
     tags$head(  HTML("
                      <style>
                      .tabScroll {
                      height:87vh;
                      padding-left:15px;
                      overflow-y: auto; 
                      overflow-x: hidden;
                      }
                      .tabScroll::-webkit-scrollbar-track
                      {
                      -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
                      background-color: #F5F5F5;
                      }
                      .tabScroll::-webkit-scrollbar
                      {
                        width: 6px;
                        background-color: #F5F5F5;
                      }
                      .tabScroll::::-webkit-scrollbar-thumb
                      {
                        background-color: #000000;
                        }
                      .boxScroll {
                      width:100%; 
                      overflow-x: auto;
                      }
/*                      .boxScroll::-webkit-scrollbar-track
                      {
                      -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
                      background-color: #F5F5F5;
                      }
                      .boxScroll::-webkit-scrollbar
                      {
                      width: 3px;
                      background-color: #F5F5F5;
                      }
                      .boxScroll::::-webkit-scrollbar-thumb
                      {
                      background-color: #000000;
                      } */
                      </style>")),
     #useShinyjs(),
     tabItems(
       tabItem(tabName = "home",
               box(
                uiOutput("loginPanel")
               ),
               box(
                title="Info",
                HTML("In case of error please check with the admin for new user name and password.")
               )),
       tabItem(tabName = "fileUpload",
               HTML('<div class="tabScroll">'),
                 column(3,
                        
                        fileInput("file1", "Choose CSV File",
                                  accept = c(
                                    "text/csv",
                                    "text/comma-separated-values,text/plain",
                                    ".csv")
                        ),
                        
                        checkboxInput("header", "Header", TRUE),
                        tags$hr(),
                        uiOutput("columnRemove"),
                        actionButton("rmColSubmit","Remove Columns",icon=icon("scissors")),
                        actionButton("rmColRevert","Undo",icon=icon("undo")),
                        downloadButton('downloadProcess', 'Download'),
                        checkboxInput("changeDataType","Change data type of the columns", value = FALSE),
                        uiOutput("dataFormat"),
                        checkboxInput("addAColumn","Create a new column", value = FALSE),
                        uiOutput("addColumn"),
                        checkboxInput("sortTable","Sort data", value = FALSE),
                        uiOutput("sortInput")
                 ),
                 column(9,
                        fluidRow(
                          box(width = 12, title = "Data view",
                              HTML('<div class="boxScroll">'),
                              uiOutput("sampleUpload"),
                              HTML('</div>'),
                              checkboxInput("sampleView","View Complete Table", value = FALSE)) 
                        ),
                        fluidRow(
                          box(width = 12, title ="Summary",
                              HTML('<div class="boxScroll" style = "padding-left:20px;">'),
                              column(11,
                                     uiOutput("summaryView")
                                     ),

                              HTML('</div>')) 
                        )
                 ),
               HTML("</div>") 
               ),
       tabItem(tabName = "eda",
               HTML('<div class="tabScroll">'),
               column(3,
                      radioButtons("edaType","EDA:", choices = c("Univariate Analysis"="uni","Bivariate Analysis"="bi")),
                      conditionalPanel(condition = "input.edaType == 'uni'",
                                       radioButtons("uniType","Distribution for:",choices = c("Numerical"="num","Factor"="fac")),
                                       conditionalPanel(condition = "input.uniType == 'num'",
                                                        uiOutput("uniNumVr")
                                                        ),
                                       conditionalPanel(condition = "input.uniType == 'fac'",
                                                        uiOutput("uniFacVr")
                                       )

                      ),
                      conditionalPanel(condition = "input.edaType == 'bi'",
                                       fluidRow(
                                         uiOutput("biVr1"),
                                         uiOutput("biVr2")
                                       )
                                       )),
               column(9,
                      box(width = 12,
                          uiOutput("edaPlotOut")
                      ),
                      box(width = 12,
                          HTML('<div class="boxScroll" style = "padding-left:20px;">'),  
                          uiOutput("edaOutput"),
                          HTML("</div>")
                          )
                      ),
               HTML("</div>")),
       tabItem(tabName = "corChartEda",
               HTML('<div class="tabScroll">'),
               h4("Corelation"),
               column(3,
                      checkboxInput("selectedVarCor","Corelation between selected variables", value = F),
                      uiOutput("corVarSel"),
                      sliderInput("corThres","Correlation threshold for network", min=0.1, max = 0.9, step = 0.1, value = 0.5)
                      ),
               column(9,
                      uiOutput("corVisuals")
                      ),
               HTML("</div>")
               ),
       tabItem(tabName = "outlierEda",
               HTML('<div class="tabScroll">'),
               h4("Outlier Detection"),
               fluidRow(
               column(3,
                      radioButtons("outlierType","Method", choices = c("Univariate","Multivariate")),
                      uiOutput("outlierVariable"),
                      conditionalPanel(condition = "input.outlierType=='Univariate'",
                                       selectInput("outlierMethod","Algorithm",choices = c("IQR","Z-Score"="z","Percentile"="p")),
                                       conditionalPanel(condition = "input.outlierMethod == 'IQR'",
                                                        sliderInput("IQRbounds","Bounds for IQR:",min = 1.5, max = 3, step = 0.1, value = 2),
                                                        HTML("1.5 is a mild threshold while 3 is the extreme threshold.")
                                       ),
                                       conditionalPanel(condition = "input.outlierMethod == 'z'",
                                                        sliderInput("zProb","Z Score Percentile:",min = 1, max = 99.5, step = 0.5, value = 95),
                                                        HTML("Set the percentile of the Z Score to filter them as outlier.")
                                       ),
                                       conditionalPanel(condition = "input.outlierMethod == 'p'",
                                                        sliderInput("pProb1","Lower Percentile:",min = 1, max = 99.5, step = 0.5, value = 5),
                                                        sliderInput("pProb2","Upper Percentile:",min = 1, max = 99.5, step = 0.5, value = 95),
                                                        HTML("Set the percentile to filter as outlier.")
                                       )
                                       ),
                      actionButton("outlierDetect","Detect Outlier", icon = icon("search"))
                      ),
               column(9,
                      uiOutput("outlierOutput")
                      )
               ), #fluid Row 1 detection
               HTML("<center>Scroll down for treatment.</center>"),
               tags$hr(),
               fluidRow(
                 h4("Outlier Treatment"),
                 column(4,
                        selectInput("outTreatType","Treatment:", choices = c("Overall"="o","Upper and Lower"="ul","Enter Value"="enter"))
                        ),
                 column(4,
                        conditionalPanel(condition = "input.outTreatType == 'o'",
                                         selectInput("outTreatOverall","Select option:",choices = c("Mean"="mean","Median"="median","Delete Rows"="delete"))
                        ),
                        conditionalPanel(condition = "input.outTreatType == 'ul'",
                                         selectInput("outTreatmaxMin","Select option:",choices = c("Max and Min"="maxMin"))
                        ),
                        
                        conditionalPanel(condition = "input.outTreatType == 'enter'",
                                         numericInput("outTreatEnter","Value to replace with:", value = 0)
                        )
                        ),
                 column(4,
                        HTML("<br>"),
                        actionButton("outTreat","Treat",icon = icon("circle-o-notch"))
                        )
               ),
               HTML("</div>")
               ),
       tabItem(tabName = "seasonEda",
               HTML('<div class="tabScroll">'),
               h4("Seasonality"),
               column(3,
                      uiOutput("seasonVarOut")
                      ),
               column(9,
                      uiOutput("seasonOutput")
                      ),
               HTML("</div>")
               ),
       tabItem(tabName = "variableSelection",
               h2("Variable Selection")),
       tabItem(tabName = "modelling",
               h2("Modelling")),
       tabItem(tabName = "settings",
               HTML('<div class="tabScroll">'),
               h2("Settings"),
               uiOutput("levelSettings"),
               HTML("</div>"))
     )
   )
  )
)

loginCheck <- function(userName, passWord){
  dirLoc <- paste("www/admin/",userName,sep = "")
  if(dir.exists(dirLoc)){
    userInfo <- read.csv(paste(dirLoc,"/info.csv",sep=""), stringsAsFactors = F)
    access <- if(userInfo$user == userName & userInfo$pass == passWord){userInfo}else{0}
  }else{
    access <- 0
  }
  return(access)
}
userCreate <- function(userName, passWord, name, level,eda, model, log, text){
  dirLoc <- paste("www/admin/",userName,sep = "")
  if(dir.exists(dirLoc)){
    success <- "User already exist. Try some other username."
  }else{
    dir.create(dirLoc)
    userInfo <- data.frame(user = userName, pass = passWord, name = name, level = level,eda = eda, model = model, log = log, text = text, stringsAsFactors = F)
    write.csv(userInfo,paste(dirLoc,"/info.csv",sep=""),row.names = F)
    if(file.exists(paste(dirLoc,"/info.csv",sep=""))){
      success <- "User created."
    }else{
      success <- "Sorry, we are facing an error. Please contact the developers."
    }
  }
  return(success)
}
editUser <- function(column, value, user){
  dirLoc <- paste("www/admin/",user,"/info.csv",sep="")
  if(file.exists(dirLoc)){
    info <- read.csv(dirLoc, stringsAsFactors = F)
    info[1,column] <- value
    write.csv(info, dirLoc, row.names = F)
    success <-"Success"
  }else{
    success <- "Info unavailable."      
  }
  return(success)
}

removeUser <- function(user){
  dirLoc <- paste("www/admin/",user,sep="")
  if(dir.exists(dirLoc)){
    unlink(dirLoc, force = T, recursive = T)
    if(dir.exists(dirLoc)){
      success <- "Unable to remove. Delete manually."
    }else{
      success <- "User Removed."      
    }
  }else{
    success <- "User does not exist."
  }
  return(success)
}
accessStatus <- function(pathname){
  status <- if(pathname == "/"){"Offline"}else{"Online"}
  return(status)
}